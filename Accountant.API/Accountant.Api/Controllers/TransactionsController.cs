﻿using System;
using System.Threading.Tasks;
using Accountant.Services.Services;
using Accountant.Services.Services.Impl;
using Microsoft.AspNetCore.Mvc;

namespace Accountant.Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class TransactionsController : ControllerBase
    {
        private readonly ITransactionService _transactionService;

        public TransactionsController(ITransactionService transactionService)
        {
            _transactionService = transactionService;
        }

        [HttpGet]
        public async Task<IActionResult> GetTransactionsAsync(int accountantId, int? expensesType,
                                                                            DateTime? date, string searchTerm,
                                                                            string orderBy, OrderDirectionType orderDirection,
                                                                            int page, int pageSize, bool? transactionType)
        {
            var transactions= await _transactionService.GetTransactionsAsync(accountantId, expensesType,
                                                                            date, searchTerm,
                                                                            orderBy, orderDirection,
                                                                            page, pageSize, transactionType);
            return Ok(transactions);
        }
    }
}
