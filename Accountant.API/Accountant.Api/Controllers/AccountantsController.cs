﻿using System.Threading.Tasks;
using Accountant.Services.Services;
using Accountant.Services.Services.Dto;
using Accountant.Services.Services.Dto.Accountant;
using Accountant.Services.Services.Impl;
using Microsoft.AspNetCore.Mvc;

namespace Accountant.Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class AccountantsController : ControllerBase
    {
        private readonly IAccountantService _accountantService;

        public AccountantsController(IAccountantService accountantService)
        {
            _accountantService = accountantService;
        }

        [HttpGet]
        public async Task<IActionResult> GetAccountantsAsync(int userId)
        {
            var accountants = await _accountantService.GetAccountantsAsync(userId);
            return Ok(accountants);
        }

        [HttpGet("all")]
        public async Task<IActionResult> GetAccountantAsync(int userId, string searchTerm, string orderBy, OrderDirectionType orderDirection, int page, int pageSize)
        {
            var accountant = await _accountantService.GetAccountantsAsync(userId, searchTerm, orderBy, orderDirection, page, pageSize);
            return Ok(accountant);
        }

        [HttpPost]
        public async Task<IActionResult> createAccountantAsync([FromBody]AccountantDto model)
        {
            var accountant = model.ToAccountant();
            await _accountantService.CreateAsync(accountant);
            return Ok();
        }
    }
}
