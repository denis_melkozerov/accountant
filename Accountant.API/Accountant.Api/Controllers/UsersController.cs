﻿using System.Threading.Tasks;
using Accountant.Core.Helpers;
using Accountant.Services.Services;
using Accountant.Services.Services.Dto;
using Accountant.Services.Services.Dto.User;
using Accountant.Services.Services.Impl;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Accountant.Api.Controllers
{
    [Authorize]
    [ApiController]
    [Route("[controller]")]
    public class UsersController : ControllerBase
    {
        private readonly IUserService _userService;

        public UsersController(IUserService userService)
        {
            _userService = userService;
        }

        [AllowAnonymous]
        [HttpPost("authenticate")]
        public async Task<IActionResult> AuthenticateAsync([FromBody]AuthenticateModel model)
        {
            var user = await _userService.AuthenticateAsync(model.Login, model.Password);

            if (user == null)
                return BadRequest(new { message = "Username or password is incorrect" });

            return Ok(user);
        }

        [AllowAnonymous]
        [HttpPost("register")]
        public async Task<IActionResult> RegisterAsync([FromBody]RegisterModel model)
        {
            var user = model.ToUser();

            try
            {
                await _userService.CreateAsync(user, model.Password);
                return Ok();
            }
            catch (AppException ex)
            {
                return BadRequest(new { message = ex.Message });
            }
        }

        [Authorize(Roles = "Admin")]
        [HttpGet]
        public async Task<IActionResult> GetAllAsync(string searchTerm, string orderBy, OrderDirectionType orderDirection, int page, int pageSize) {

            var users = await _userService.GetAllAsync(searchTerm, orderBy, orderDirection, page, pageSize);
            return Ok(users);
        }
    }
}
