﻿using System.Threading.Tasks;
using Accountant.Services.Services;
using Accountant.Services.Services.Dto.Role;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Accountant.Api.Controllers
{
    [Authorize]
    [ApiController]
    [Route("[controller]")]
    public class RolesController : ControllerBase
    {
        private readonly IRoleService _roleService;

        public RolesController(IRoleService roleService)
        {
            _roleService = roleService;
        }

        [Authorize(Roles = "Admin")]
        [HttpGet]
        public async Task<IActionResult> GetRolesAsync()
        {
            var users = await _roleService.GetRolesAsync();
            return Ok(users);
        }

        [Authorize(Roles = "Admin")]
        [HttpPost]
        public async Task<IActionResult> UpdateRolesAsync([FromBody]UpdateRolesModel newUserRoles)
        {

            var roles = await _roleService.UpdateRolesAsync(newUserRoles);
            return Ok(roles);
        }
    }
}
