﻿using System.Threading.Tasks;
using Accountant.Services.Services;
using Microsoft.AspNetCore.Mvc;

namespace Accountant.Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ExpensesTypesController : ControllerBase
    {
        private readonly IExpensesTypeService _expensesTypeService;

        public ExpensesTypesController(IExpensesTypeService expensesTypeService)
        {
            _expensesTypeService = expensesTypeService;
        }

        [HttpGet]
        public async Task<IActionResult> GetExpensesTypesAsync()
        {
            var expensesTypes = await _expensesTypeService.GetExpensesTypesAsync();
            return Ok(expensesTypes);
        }
    }
}
