﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Accountant.Data.Entities
{
    public class User
    {
        public int Id { get; set; }

        [Required]
        [StringLength(250)]
        public string FirstName { get; set; }

        [Required]
        [StringLength(250)]
        public string LastName { get; set; }

        [Required]
        [StringLength(250)]
        public string Login { get; set; }

        [Required]
        [StringLength(250)]
        public string Email { get; set; }

        [Required]
        public byte[] PasswordHash { get; set; }

        [Required]
        public byte[] Salt { get; set; }

        public virtual ICollection<UserRole> Roles { get; set; }
    }
}
