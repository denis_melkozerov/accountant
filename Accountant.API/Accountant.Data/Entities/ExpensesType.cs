﻿using System.ComponentModel.DataAnnotations;

namespace Accountant.Data.Entities
{
    public class ExpensesType
    {
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public virtual Transaction Transaction { get; set; }
    }
}
