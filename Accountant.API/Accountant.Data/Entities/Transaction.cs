﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Accountant.Data.Entities
{
    public class Transaction
    {
        public int Id { get; set; }

        [Required]
        public double Size { get; set; }

        [Required]
        public DateTime DateOfTransaction { get; set; }

        [Required]
        public bool TransactionType { get; set; }

        [Required]
        public int AccountId { get; set; }

        [Required]
        public virtual Accountant Account { get; set; }

        [Required]
        public int ExpensesTypeId { get; set; }

        [Required]
        public virtual ExpensesType ExpensesType { get; set; }
    }
}
