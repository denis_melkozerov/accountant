﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Accountant.Data.Entities
{
    public class Role
    {
        public int Id { get; set; }

        [Required]
        [StringLength(250)]
        public string Name { get; set; }

        public virtual ICollection<UserRole> Users { get; set; }
    }
}
