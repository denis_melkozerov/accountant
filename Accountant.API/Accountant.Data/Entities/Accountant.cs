﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Accountant.Data.Entities
{
    public class Accountant
    {
        public int Id { get; set; }

        [Required]
        [StringLength(250)]
        public string Number { get; set; }

        [Required]
        public double Balance { get; set; }

        [Required]
        public int UserId { get; set; }

        public virtual User User { get; set; }

        [Required]
        public DateTime DateCreated { get; set; }

        public virtual ICollection<Transaction> Transactions { get; set; }
    }
}
