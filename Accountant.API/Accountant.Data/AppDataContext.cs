﻿using Accountant.Data.Entities;
using Microsoft.EntityFrameworkCore;

namespace Accountant.Data
{
    public class AppDataContext : DbContext
    {
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<Role> Roles { get; set; }
        public virtual DbSet<UserRole> UserRoles { get; set; }
        public virtual DbSet<Entities.Accountant> Accountants { get; set; }
        public virtual DbSet<ExpensesType> ExpensesTypes { get; set; }
        public virtual DbSet<Transaction> Transactions { get; set; }

        public AppDataContext(DbContextOptions<AppDataContext> options)
            : base(options)
        {
            //Database.EnsureDeleted();
            //Database.EnsureCreated();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<UserRole>()
                .HasKey(t => new { t.UserId, t.RoleId });
        }
    }
}
