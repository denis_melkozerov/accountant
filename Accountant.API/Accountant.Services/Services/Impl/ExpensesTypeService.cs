﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Accountant.Data;
using Accountant.Services.Services.Dto;
using Accountant.Services.Services.Dto.Transaction;
using Microsoft.EntityFrameworkCore;

namespace Accountant.Services.Services.Impl
{
    public class ExpensesTypeService : IExpensesTypeService
    {
        private readonly AppDataContext _context;

        public ExpensesTypeService(AppDataContext context)
        {
            _context = context;
        }

        public async Task<List<ExpensesTypeDto>> GetExpensesTypesAsync()
        {
            var expensesTypes = await _context.ExpensesTypes
                .Select(e => e.ToExpensesTypeDto())
                .ToListAsync();

            return expensesTypes;
        }
    }
}
