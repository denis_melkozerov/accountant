﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Accountant.Data;
using Accountant.Data.Entities;
using Accountant.Services.Services.Dto;
using Accountant.Services.Services.Dto.Search;
using Accountant.Services.Services.Dto.Transaction;
using Microsoft.EntityFrameworkCore;

namespace Accountant.Services.Services.Impl
{
    public class TransactionService : ITransactionService
    {
        private readonly AppDataContext _context;

        public TransactionService(AppDataContext context)
        {
            _context = context;
        }

        public async Task<ResultModel<TransactionDto>> GetTransactionsAsync(int accountantId, int? expensesType,
                                                                            DateTime? date, string searchTerm,
                                                                            string orderBy, OrderDirectionType orderDirection,
                                                                            int page, int pageSize, bool? transactionType)
        {
            searchTerm = searchTerm == null ? "" : searchTerm;
            var validColumns = new[]
            {
                nameof(Transaction.Id),
                nameof(Transaction.Size),
                nameof(Transaction.DateOfTransaction),
                nameof(Transaction.TransactionType),
                nameof(Transaction.ExpensesType)
            };

            var columnName =
                validColumns.Contains(orderBy, StringComparer.OrdinalIgnoreCase)
                ? orderBy
                : validColumns.First();

            var query = _context.Transactions
                .Include(u => u.ExpensesType)
                .Where(u => (u.Id.ToString().Contains(searchTerm) ||
                            u.Size.ToString().Contains(searchTerm) ||
                            u.DateOfTransaction.ToString().Contains(searchTerm) ||
                            u.ExpensesType.Name.Contains(searchTerm)) &&
                            u.AccountId == accountantId &&
                            u.TransactionType != transactionType &&
                            u.ExpensesTypeId == expensesType)
                .AsNoTracking();

            var totalRecords = await query.CountAsync();
            var records = await query
                .OrderBy(columnName, orderDirection == OrderDirectionType.Asc)
                .Skip(page * pageSize)
                .Take(pageSize)
                .Select(t => t.ToTransactionDto())
                .ToListAsync();

            return new ResultModel<TransactionDto> { Data = records, Count = totalRecords };
        }
    }
}
