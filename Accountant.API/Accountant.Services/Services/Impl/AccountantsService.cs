﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Accountant.Data;
using Accountant.Services.Services.Dto;
using Accountant.Services.Services.Dto.Accountant;
using Accountant.Services.Services.Dto.Search;
using Microsoft.EntityFrameworkCore;

namespace Accountant.Services.Services.Impl
{
    public class AccountantsService : IAccountantService
    {
        private readonly AppDataContext _context;

        public AccountantsService(AppDataContext context)
        {
            _context = context;
        }

        public async Task<List<AccountantsListDto>> GetAccountantsAsync(int userId)
        {
            var accounts = await _context.Accountants
                .Where(a => a.UserId == userId)
                .Select(a => a.ToAccountantsListDto())
                .ToListAsync();

            return accounts;
        }

        public async Task<ResultModel<AccountantDto>> GetAccountantsAsync(int userId, string searchTerm, string orderBy, OrderDirectionType orderDirection, int page, int pageSize)
        {
            searchTerm = searchTerm == null ? "" : searchTerm;
            var validColumns = new[]
            {
                nameof(Accountant.Data.Entities.Accountant.Id)
            };

            var columnName =
                validColumns.Contains(orderBy, StringComparer.OrdinalIgnoreCase)
                ? orderBy
                : validColumns.First();

            var query = _context.Accountants
                .Where(u => u.Id.ToString().Contains(searchTerm) &&
                            u.UserId == userId)
                .AsNoTracking();

            var totalRecords = await query.CountAsync();
            var accounts = await query
                .OrderBy(columnName, orderDirection == OrderDirectionType.Asc)
                .Skip(page * pageSize)
                .Take(pageSize)
                .Select(u => u.ToAccountantDto())
                .ToListAsync();

            return new ResultModel<AccountantDto> { Data = accounts, Count = totalRecords };
        }

        public async Task<Data.Entities.Accountant> CreateAsync(Data.Entities.Accountant account)
        {
            account.DateCreated = new DateTime();
            await _context.Accountants.AddAsync(account);
            _context.SaveChanges();

            return account;
        }
    }
}
