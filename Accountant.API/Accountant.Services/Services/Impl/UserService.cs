﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Accountant.Core.Helpers;
using Accountant.Data;
using Accountant.Data.Entities;
using Accountant.Services.Services.Dto;
using Accountant.Services.Services.Dto.Search;
using Accountant.Services.Services.Dto.User;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;

namespace Accountant.Services.Services.Impl
{
    public class UserService : IUserService
    {
        private readonly AppSettings _appSettings;
        private readonly AppDataContext _context;

        public UserService(IOptions<AppSettings> appSettings, AppDataContext context)
        {
            _appSettings = appSettings.Value;
            _context = context;
        }

        public async Task<UserDto> AuthenticateAsync(string username, string password)
        {
            var user = await _context.Users.Where(u => u.Login == username || u.Email == username)
                .Include(u => u.Roles)
                .ThenInclude(r => r.Role)
                .FirstOrDefaultAsync();

            if (user == null)
                throw new ArgumentException("User not found.", "User");

            if (!AuthenticationHelper.VerifyPasswordHash(password, user.PasswordHash, user.Salt))
                return null;

            var currentUser = user.ToUserDto();
            currentUser.Token = AuthenticationHelper.CreateToken(user.Id, currentUser.Roles.Select(r => r.Name), _appSettings.Secret);

            return currentUser;
        }

        public async Task<User> CreateAsync(User user, string password)
        {
            if (string.IsNullOrWhiteSpace(password))
                throw new AppException("Password is required");

            if (_context.Users.Any(x => x.Login == user.Login))
                throw new AppException("Login \"" + user.Login + "\" is already taken");

            if (_context.Users.Any(x => x.Email == user.Email))
                throw new AppException("Email \"" + user.Email + "\" is already taken");

            byte[] passwordHash, salt;
            AuthenticationHelper.CreatePasswordHash(password, out passwordHash, out salt);

            user.PasswordHash = passwordHash;
            user.Salt = salt;

            await _context.Users.AddAsync(user);
            _context.SaveChanges();

            return user;
        }

        public async Task<ResultModel<UserDto>> GetAllAsync(string searchTerm, string orderBy, OrderDirectionType orderDirection, int page, int pageSize)
        {
            searchTerm = searchTerm == null ? "" : searchTerm;
            var validColumns = new[]
            {
                nameof(User.Id),
                nameof(User.FirstName),
                nameof(User.LastName),
                nameof(User.Email),
            };

            var columnName =
                validColumns.Contains(orderBy, StringComparer.OrdinalIgnoreCase)
                ? orderBy
                : validColumns.First();

            var query = _context.Users
                .Include(u => u.Roles)
                .ThenInclude(r => r.Role)
                .Where(u => u.Id.ToString().Contains(searchTerm) ||
                            u.FirstName.Contains(searchTerm) ||
                            u.LastName.Contains(searchTerm) ||
                            u.Email.Contains(searchTerm) ||
                            u.Roles.Where(r => r.Role.Name.Contains(searchTerm)).Any())
                .AsNoTracking();

            var totalRecords = await query.CountAsync();
            var records = await query
                .OrderBy(columnName, orderDirection == OrderDirectionType.Asc)
                .Skip(page * pageSize)
                .Take(pageSize)
                .Select(u => u.ToUserDto())
                .ToListAsync();

            return new ResultModel<UserDto> { Data = records, Count = totalRecords };
        }
    }

    public enum OrderDirectionType
    {
        Asc,
        Desc,
    }


    public static class IQueryableExtensions
    {
        public static IQueryable<TSource> OrderBy<TSource>(this IQueryable<TSource> query, string key, bool ascending = true)
        {
            if (string.IsNullOrWhiteSpace(key))
            {
                return query;
            }

            var lambda = (dynamic)CreateExpression(typeof(TSource), key);

            return ascending
                ? Queryable.OrderBy(query, lambda)
                : Queryable.OrderByDescending(query, lambda);
        }

        private static LambdaExpression CreateExpression(Type type, string propertyName)
        {
            var param = Expression.Parameter(type, "x");

            Expression body = param;
            foreach (var member in propertyName.Split('.'))
            {
                body = Expression.PropertyOrField(body, member);
            }

            return Expression.Lambda(body, param);
        }
    }
}
