﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Accountant.Data;
using Accountant.Data.Entities;
using Accountant.Services.Services.Dto;
using Accountant.Services.Services.Dto.Role;
using Microsoft.EntityFrameworkCore;

namespace Accountant.Services.Services.Impl
{
    public class RoleService : IRoleService
    {
        private readonly AppDataContext _context;

        public RoleService(AppDataContext context)
        {
            _context = context;
        }

        public async Task<List<RoleDto>> GetRolesAsync()
        {
            var roles = await _context.Roles
                .Select(r => r.ToRoleDto())
                .ToListAsync();

            return roles;
        }

        public async Task<List<RoleDto>> UpdateRolesAsync(UpdateRolesModel newUserRoles)
        {
            var oldUserRoles = await _context.UserRoles
                .Where(ur => ur.UserId == newUserRoles.UserId)
                .ToListAsync();

            List<UserRole> removeRoles = new List<UserRole>();
            List<UserRole> addRoles = new List<UserRole>();

            foreach (var newUserRoleIds in newUserRoles.RoleIds)
            {
                foreach (var oldUserRole in oldUserRoles)
                {
                    if (oldUserRole.RoleId == newUserRoleIds)
                    {
                        _context.UserRoles.Remove(new UserRole { UserId = newUserRoles.UserId, RoleId = newUserRoleIds });
                    }
                }
            }

            foreach (var oldUserRole in oldUserRoles)
            {
                foreach (var newUserRoleIds in newUserRoles.RoleIds)
                {
                    if (oldUserRole.RoleId == newUserRoleIds)
                    {
                        _context.UserRoles.Add(new UserRole { UserId = newUserRoles.UserId, RoleId = newUserRoleIds });
                    }
                }
            }

            //_context.UserRoles.RemoveRange(removeRoles);
            //await _context.UserRoles.AddRangeAsync(addRoles);

            var roles = await _context.Roles
                .Select(r => r.ToRoleDto())
                .ToListAsync();

            return roles;
        }
    }
}
