﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Accountant.Services.Services.Dto.Accountant;
using Accountant.Services.Services.Dto.Search;
using Accountant.Services.Services.Impl;

namespace Accountant.Services.Services
{
    public interface IAccountantService
    {
        Task<List<AccountantsListDto>> GetAccountantsAsync(int userId);

        Task<ResultModel<AccountantDto>> GetAccountantsAsync(int userId, string searchTerm, string orderBy, OrderDirectionType orderDirection, int page, int pageSize);

        Task<Data.Entities.Accountant> CreateAsync(Data.Entities.Accountant account);
    }
}
