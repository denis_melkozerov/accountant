﻿using System;
using System.Threading.Tasks;
using Accountant.Services.Services.Dto.Search;
using Accountant.Services.Services.Dto.Transaction;
using Accountant.Services.Services.Impl;

namespace Accountant.Services.Services
{
    public interface ITransactionService
    {
        Task<ResultModel<TransactionDto>> GetTransactionsAsync(int accountantId, int? expensesType,
                                                                            DateTime? date, string searchTerm,
                                                                            string orderBy, OrderDirectionType orderDirection,
                                                                            int page, int pageSize, bool? transactionType);
    }
}
