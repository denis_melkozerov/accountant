﻿using System.ComponentModel.DataAnnotations;

namespace Accountant.Services.Services.Dto.User
{
    public class AuthenticateModel
    {
        [Required]
        public string Login { get; set; }

        [Required]
        public string Password { get; set; }
    }
}
