﻿using System.Collections.Generic;
using Accountant.Services.Services.Dto.Role;

namespace Accountant.Services.Services.Dto.User
{
    public class UserDto
    {
        public int Id { get; set; }
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Login { get; set; }

        public string Email { get; set; }

        public virtual IEnumerable<RoleDto> Roles { get; set; }

        public string Token { get; set; }
    }
}
