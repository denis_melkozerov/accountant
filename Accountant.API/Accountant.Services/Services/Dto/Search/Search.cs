﻿using System.ComponentModel.DataAnnotations;

namespace Accountant.Services.Services.Dto.Search
{
    public class Search
    {
        [Required]
        public string SearchTerm { get; set; }

        [Required]
        public string OrderBy { get; set; }

        [Required]
        public string Order { get; set; }

        [Required]
        public string PageIndex { get; set; }

        [Required]
        public string PageSize { get; set; }
    }
}
