﻿using System.Collections.Generic;

namespace Accountant.Services.Services.Dto.Search
{
    public class ResultModel<T>
    {
        public IEnumerable<T> Data { get; set; }

        public int Count { get; set; }
    }
}
