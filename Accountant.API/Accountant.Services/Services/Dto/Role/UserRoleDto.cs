﻿namespace Accountant.Services.Services.Dto.Role
{
    public class UserRoleDto
    {
        public int UserId { get; set; }
        public int RoleId { get; set; }
    }
}
