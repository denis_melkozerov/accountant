﻿using System.Collections.Generic;

namespace Accountant.Services.Services.Dto.Role
{
    public class UpdateRolesModel
    {
        public int UserId { get; set; }

        public IEnumerable<int> RoleIds { get; set; }
    }
}
