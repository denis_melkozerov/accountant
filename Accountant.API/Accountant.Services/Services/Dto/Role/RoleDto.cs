﻿namespace Accountant.Services.Services.Dto.Role
{
    public class RoleDto
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}
