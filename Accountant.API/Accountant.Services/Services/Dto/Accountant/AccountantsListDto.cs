﻿namespace Accountant.Services.Services.Dto.Accountant
{
    public class AccountantsListDto
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}
