﻿using System;

namespace Accountant.Services.Services.Dto.Accountant
{
    public class AccountantDto
    {
        public int Id { get; set; }

        public string Number { get; set; }

        public double Balance { get; set; }

        public int UserId { get; set; }

        public DateTime DateCreated { get; set; }
    }
}
