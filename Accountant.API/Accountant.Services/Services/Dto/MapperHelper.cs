﻿using System.Linq;
using AutoMapper;
using Accountant.Services.Services.Dto.User;
using Accountant.Services.Services.Dto.Role;
using Accountant.Services.Services.Dto.Transaction;

using UserDB = Accountant.Data.Entities.User;
using RoleDB = Accountant.Data.Entities.Role;
using ExpensesTypeDB = Accountant.Data.Entities.ExpensesType;
using TransactionDB = Accountant.Data.Entities.Transaction;
using AccountantDB = Accountant.Data.Entities.Accountant;
using Accountant.Services.Services.Dto.Accountant;

namespace Accountant.Services.Services.Dto
{
    public static class MapperHelper
    {
        public static IMapper _mapper;
        static MapperHelper()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<RegisterModel, UserDB>();
                cfg.CreateMap<RoleDB, RoleDto>();
                cfg.CreateMap<UserDB, UserDto>()
                    .ForMember(x => x.Roles, opt => opt.MapFrom(x => x.Roles
                    .Select(y => y.Role.ToRoleDto()).AsEnumerable()));

                cfg.CreateMap<ExpensesTypeDB, ExpensesTypeDto>();
                cfg.CreateMap<TransactionDB, TransactionDto>()
                    .ForMember(x => x.ExpensesType, opt => opt.MapFrom(x => x.ExpensesType.ToExpensesTypeDto()));
                cfg.CreateMap<TransactionDto, TransactionDB>();
                cfg.CreateMap<AccountantDB, AccountantDto>();
                cfg.CreateMap<AccountantDto, AccountantDB>();

                cfg.CreateMap<AccountantDB, AccountantsListDto>()
                .ForMember(x => x.Name, opt => opt.MapFrom(x => x.Number)); ;
            });
            _mapper = config.CreateMapper();
        }

        public static UserDB ToUser(this RegisterModel model)
        {
            return _mapper.Map<UserDB>(model);
        }

        public static RoleDto ToRoleDto(this RoleDB model)
        {
            return _mapper.Map<RoleDto>(model);
        }

        public static UserDto ToUserDto(this UserDB model)
        {
            return _mapper.Map<UserDto>(model);
        }

        public static ExpensesTypeDto ToExpensesTypeDto(this ExpensesTypeDB model)
        {
            return _mapper.Map<ExpensesTypeDto>(model);
        }

        public static TransactionDto ToTransactionDto(this TransactionDB model)
        {
            return _mapper.Map<TransactionDto>(model);
        }

        public static TransactionDB ToTransaction(this TransactionDto model)
        {
            return _mapper.Map<TransactionDB>(model);
        }

        public static AccountantDto ToAccountantDto(this AccountantDB model)
        {
            return _mapper.Map<AccountantDto>(model);
        }

        public static AccountantDB ToAccountant(this AccountantDto model)
        {
            return _mapper.Map<AccountantDB>(model);
        }

        public static AccountantsListDto ToAccountantsListDto(this AccountantDB model)
        {
            return _mapper.Map<AccountantsListDto>(model);
        }
    }
}
