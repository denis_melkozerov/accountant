﻿namespace Accountant.Services.Services.Dto.Transaction
{
    public class ExpensesTypeDto
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}
