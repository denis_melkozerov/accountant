﻿using System;

namespace Accountant.Services.Services.Dto.Transaction
{
    public class TransactionDto
    {
        public int Id { get; set; }

        public double Size { get; set; }

        public DateTime DateOfTransaction { get; set; }

        public bool TransactionType { get; set; }

        public ExpensesTypeDto ExpensesType { get; set; }
    }
}
