﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Accountant.Services.Services.Dto.Role;

namespace Accountant.Services.Services
{
    public interface IRoleService
    {
        Task<List<RoleDto>> GetRolesAsync();
        Task<List<RoleDto>> UpdateRolesAsync(UpdateRolesModel newUserRoles);
    }
}
