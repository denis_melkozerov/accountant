﻿using System.Threading.Tasks;
using Accountant.Data.Entities;
using Accountant.Services.Services.Dto.Search;
using Accountant.Services.Services.Dto.User;
using Accountant.Services.Services.Impl;

namespace Accountant.Services.Services
{
    public interface IUserService
    {
        Task<UserDto> AuthenticateAsync(string username, string password);

        Task<User> CreateAsync(User user, string password);

        Task<ResultModel<UserDto>> GetAllAsync(string searchTerm, string orderBy, OrderDirectionType orderDirection, int page, int pageSize);
    }
}
