﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Accountant.Services.Services.Dto.Transaction;

namespace Accountant.Services.Services
{
    public interface IExpensesTypeService
    {
        Task<List<ExpensesTypeDto>> GetExpensesTypesAsync();
    }
}
