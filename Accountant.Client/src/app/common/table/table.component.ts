import { Component, OnInit, Input, ViewChild, SimpleChanges, OnChanges, TemplateRef, AfterViewInit, EventEmitter, Output } from '@angular/core';
import { MatSort, MatTableDataSource, MatPaginator } from '@angular/material';
import { merge } from 'rxjs';
import { TableProperty, TableDatas } from '@app/_models';

@Component({
    selector: 'app-table',
    templateUrl: './table.component.html',
    styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit, OnChanges, AfterViewInit {

    private loading: boolean = true;
    private searchTerm: string = "";
    private length: number;
    private displayedColumns: string[] = [];
    private dataSource = new MatTableDataSource();
    private searchTermEmitter: EventEmitter<string> = new EventEmitter();
    private tableProperty: TableProperty = new TableProperty();

    @Input() templateRef: TemplateRef<any>;
    @Input() columns: Array<any>;
    @Input() tableData: TableDatas<any>;
    @ViewChild(MatSort, { static: true }) sort: MatSort;
    @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

    @Output() onChanged = new EventEmitter<TableProperty>();

    constructor() { }

    ngOnInit(): void {
        this.displayedColumns = this.columns.map(x => x.columnDef);
        this.dataSource.sort = this.sort;
    }

    ngOnChanges(changes: SimpleChanges): void {
        if (this.tableData != undefined) {
            this.dataSource.data = this.tableData.data;
            this.length = this.tableData.count;
            this.loading = false;
        }
    }

    ngAfterViewInit(): void {
        merge(this.searchTermEmitter, this.sort.sortChange)
        .subscribe(() => this.paginator.pageIndex = 0);

        merge(this.searchTermEmitter, this.sort.sortChange, this.paginator.page)
        .subscribe(() => {
            this.tableProperty.searchTerm = this.searchTerm;
            this.tableProperty.orderBy = this.sort.active;
            this.tableProperty.order = this.sort.direction.length ? this.sort.direction : 'asc';
            this.tableProperty.pageIndex = this.paginator.pageIndex;
            this.tableProperty.pageSize = this.paginator.pageSize;
            this.onChanged.emit(this.tableProperty);
        });
    }

    searchTermChange() {
        this.searchTermEmitter.next();
    }

    searchTermClear() {
        this.searchTerm = '';
        this.searchTermEmitter.next();
    }
}
