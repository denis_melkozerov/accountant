import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { AuthenticationService } from '@app/_services';
import { Router } from '@angular/router';
import { first } from 'rxjs/operators';
import { RegisterService } from '@app/_services/register.service';

@Component({
    selector: 'app-register',
    templateUrl: './register.component.html',
    styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

    registerForm: FormGroup;
    isProgress = false;

    constructor(private formBuilder: FormBuilder,
        private router: Router,
        private authenticationService: AuthenticationService,
        private registerService: RegisterService) {

        if (this.authenticationService.currentUserValue) {
            this.router.navigate(['/']);
        }
    }

    ngOnInit(): void {
        this.createForm();
    }

    onSubmit() {
        if (this.registerForm.invalid) {
            return;
        }

        this.isProgress = true;
        this.registerService.register(this.registerForm.value)
            .pipe(first())
            .subscribe(
                data => {
                    this.router.navigate(['/login']);
                    this.isProgress = false;
                },
                error => {
                    console.log(error);
                });
    }

    get formControls() {
        return this.registerForm.controls;
    }

    createForm() {
        this.registerForm = this.formBuilder.group({
            FirstName: new FormControl('', [
                Validators.required
            ]),
            LastName: new FormControl('', [
                Validators.required
            ]),
            Login: new FormControl('', [
                Validators.required
            ]),
            Email: new FormControl('', [
                Validators.required,
                Validators.email
            ]),
            Password: new FormControl('', [
                Validators.required,
                Validators.minLength(6)
            ])
        });
    }
}
