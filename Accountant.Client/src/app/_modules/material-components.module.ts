import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatAutocompleteModule, MatButtonModule, MatCardModule, 
    MatCheckboxModule, MatDatepickerModule, MatDialogModule,
    MatGridListModule, MatIconModule, MatInputModule, MatListModule,
    MatMenuModule, MatNativeDateModule, MatProgressBarModule, 
    MatProgressSpinnerModule, MatRippleModule, MatSelectModule,
    MatSidenavModule, MatSliderModule, MatSlideToggleModule, 
    MatSnackBarModule, MatTabsModule, MatToolbarModule, MatTooltipModule, MatTableModule, MatSortModule, MatPaginatorModule, MatChipsModule
} from '@angular/material';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule } from '@angular/forms';

@NgModule({
    declarations: [],
    imports: [ CommonModule ],
    exports: [
        MatInputModule,
        MatTabsModule,
        MatIconModule,
        MatButtonModule,
        MatToolbarModule,
        MatDialogModule,
        MatMenuModule,
        MatGridListModule,
        MatCardModule,
        MatSnackBarModule,
        MatTooltipModule,
        MatSliderModule,
        MatAutocompleteModule,
        MatDatepickerModule,
        MatSlideToggleModule,
        MatSidenavModule,
        MatCheckboxModule,
        MatNativeDateModule,
        MatProgressBarModule,
        MatProgressSpinnerModule,
        MatSelectModule,
        MatRippleModule,
        FlexLayoutModule,
        MatTableModule,
        MatSortModule,
        MatPaginatorModule,
        FormsModule,
        MatChipsModule
    ],
    providers: [],
})
export class MaterialComponentsModule {}