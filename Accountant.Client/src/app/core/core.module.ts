import { NgModule } from '@angular/core';
import { CoreComponent } from './core.component';
import { AppRoutingModule } from '../app-routing.module';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import { JwtInterceptor, ErrorInterceptor } from '@app/_helpers';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialComponentsModule } from '@app/_modules';
import { ToolbarComponent } from './toolbar/toolbar.component';
import { LoginComponent, RegisterComponent } from '@app/public';
import { HomeComponent, UsersComponent, AddAccountantComponent, EditUserRolesComponent, AccountantsComponent, TransactionsComponent, EditUserComponent, AddTransactionComponent } from '@app/protected';
import { TableComponent } from '@app/common';
import { AccountantService, UserService, TransactionService, RoleService, ExpensesTypeService } from '@app/_services';
import { DatePipe } from '@angular/common';
import {NgxChartsModule} from '@swimlane/ngx-charts';

@NgModule({
    declarations: [
        ToolbarComponent,
        CoreComponent,
        LoginComponent,
        RegisterComponent,
        TableComponent,
        HomeComponent,
        UsersComponent,
        EditUserComponent,
        EditUserRolesComponent,
        AccountantsComponent,
        AddAccountantComponent,
        TransactionsComponent,
        AddTransactionComponent
    ],
    imports: [
        BrowserModule,
        HttpClientModule,
        ReactiveFormsModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        MaterialComponentsModule,
        NgxChartsModule
    ],
    entryComponents: [
        AddAccountantComponent,
        EditUserComponent,
        EditUserRolesComponent,
        AddTransactionComponent
    ],
    exports: [ CoreComponent ],
    providers: [
        { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
        { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
        UserService,
        AccountantService,
        TransactionService,
        ExpensesTypeService,
        RoleService,
        DatePipe
    ]
})
export class CoreModule {}