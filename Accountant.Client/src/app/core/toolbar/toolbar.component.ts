import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from '@app/_services';
import { MatDialog } from '@angular/material';
import { EditUserComponent } from '@app/protected/home/users/edit-user/edit-user.component';

@Component({
    selector: 'app-toolbar',
    templateUrl: './toolbar.component.html',
    styleUrls: ['./toolbar.component.scss']
})
export class ToolbarComponent implements OnInit {

    private currentUser;

    constructor(private router: Router,
                public editUserDialog: MatDialog,
                private authenticationService: AuthenticationService) {

            this.authenticationService.currentUser.subscribe(u => this.currentUser = u);
        }

    ngOnInit(): void { }

    editUser() {
        const addAccountDialogRef = this.editUserDialog.open(EditUserComponent, {
            disableClose: true
        });

        addAccountDialogRef.afterClosed().subscribe(result => {
        });
    }

    logout() {
        this.authenticationService.logout();
        this.router.navigate(['/login']);
    }
}
