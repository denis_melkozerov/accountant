import { Component, OnInit } from '@angular/core';
import { loginAnimation } from '@app/_animations';
import { AuthenticationService } from '@app/_services';

@Component({
    selector: 'app-root',
    templateUrl: './core.component.html',
    styleUrls: ['./core.component.scss'],
    animations: [loginAnimation]

})
export class CoreComponent implements OnInit {

    private currentUser;

    constructor(private authenticationService: AuthenticationService) {
        this.authenticationService.currentUser.subscribe(u => this.currentUser = u);}

    ngOnInit(): void { }
}
