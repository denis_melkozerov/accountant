import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '@environments/environment';

@Injectable()
export class ExpensesTypeService {

    constructor(private http: HttpClient) { }

    getExpensesTypes() {
        return this.http.get(`${environment.apiUrl}expensestypes`);
    }
}