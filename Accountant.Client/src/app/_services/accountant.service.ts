import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from '@environments/environment';
import { Accountant } from '@app/_models';

@Injectable()
export class AccountantService {
    
    constructor(private http: HttpClient) { }

    getAccountants(userId: number, searchTerm: string = '', orderBy: string = 'id', order: string = 'asc', pageIndex: number = 0, pageSize: number = 5) {
        let params = new HttpParams()
                        .set('userId', userId.toString())
                        .set('searchTerm', searchTerm)
                        .set('orderBy', orderBy)
                        .set('orderDirection', order)
                        .set('page', pageIndex.toString())
                        .set('pageSize', pageSize.toString());

        return this.http.get(`${environment.apiUrl}accountants/all`, { params });
    }

    getAccountantsList(userId: number) {
        let params = new HttpParams()
                            .set('userId', userId.toString());
        return this.http.get(`${environment.apiUrl}accountants`, { params });
    }

    createAccountant(accountant: Accountant) {
        return this.http.post(`${environment.apiUrl}accountants`, accountant);
    }
}