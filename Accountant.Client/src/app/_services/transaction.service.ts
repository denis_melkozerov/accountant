import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from '@environments/environment';

@Injectable()
export class TransactionService {
        
    constructor(private http: HttpClient) { }

    getTransactions(accountantId: number = 1,
                    date: string, 
                    transactionType: boolean = null, 
                    expressionType: number,
                    searchTerm: string = '', 
                    orderBy: string = 'id', 
                    order: string = 'asc', 
                    pageIndex: number = 0, 
                    pageSize: number = 5) {

        let params = new HttpParams()
                        .set('accountantId', accountantId.toString())
                        .set('date', date)
                        .set('searchTerm', searchTerm)
                        .set('orderBy', orderBy)
                        .set('orderDirection', order)
                        .set('page', pageIndex.toString())
                        .set('pageSize', pageSize.toString());
        
        if(transactionType != null) {
            params.set('transactionType', transactionType != null ? transactionType.toString() : null)
        }

        return this.http.get(`${environment.apiUrl}transactions`, { params });
    }
}