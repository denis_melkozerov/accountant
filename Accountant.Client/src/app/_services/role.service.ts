import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '@environments/environment';
import { UpdateUserRoles } from '@app/_models';

@Injectable()
export class RoleService {
    
    constructor(private http: HttpClient) { }

    getRoles() {
        return this.http.get(`${environment.apiUrl}roles`);
    }

    updateRoles(data: UpdateUserRoles) {
        return this.http.post(`${environment.apiUrl}roles`, data);
    }
}