import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from '@environments/environment';

@Injectable()
export class UserService {

    constructor(private http: HttpClient) { }

    getUsers(searchTerm: string = '', orderBy: string = 'id', order: string = 'asc', pageIndex: number = 0, pageSize: number = 5) {
        let params = new HttpParams()
                        .set('searchTerm', searchTerm)
                        .set('orderBy', orderBy)
                        .set('orderDirection', order)
                        .set('page', pageIndex.toString())
                        .set('pageSize', pageSize.toString());

        return this.http.get(`${environment.apiUrl}users`, { params });
    }
}