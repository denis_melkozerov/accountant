export class UpdateUserRoles {

    constructor() {
        this.roleIds = new Array<number>();
    }

    userId: number;
    roleIds: Array<number>;
}