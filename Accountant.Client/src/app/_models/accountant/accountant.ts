import { Transaction } from '../transaction/transaction';

export class Accountant {
    id: number;
    number: string;
    balance: number;
    userId: number;
    dateCreated: Date;
    transactions: Array<Transaction>;
}