import { Roles } from '../role/roles';

export class User {
    id: number;
    firstName: string;
    lastName: string;
    login: string;
    email: string;
    password: string;
    roles: Array<Roles>;
    token: string;
}