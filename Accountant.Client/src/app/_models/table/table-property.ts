export class TableProperty {
    searchTerm: string;
    orderBy: string;
    order: string;
    pageIndex: number;
    pageSize: number;
}