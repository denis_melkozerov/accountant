import { ExpensesType } from '../expenses-type/expenses-type';

export class Transaction {
    id: number;
    size: number;
    dateOfTransaction: Date;
    transactionType: boolean;
    expensesType: ExpensesType;
}