import { trigger, state, style, transition, animate } from '@angular/animations';

export const loginAnimation =
    trigger('loginAnimation', [
        state('pull', style({
            'width': '40%',
            'height': '450px'
        })),
        state('full', style({
            'width': '100%',
            'height': 'calc(100% - 32px)'
        })),
        transition('pull => full', [
            animate('.2s')
        ]),
        transition('full => pull', [
            animate('.2s')
        ]),
    ]);