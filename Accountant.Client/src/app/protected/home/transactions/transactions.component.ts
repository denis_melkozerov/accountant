import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { TableDatas, Transaction, TableProperty, FilterModel } from '@app/_models';
import { TransactionService, AccountantService, ExpensesTypeService, AuthenticationService } from '@app/_services';
import { DatePipe } from '@angular/common';
import { AddTransactionComponent } from './add-transaction/add-transaction.component';
import { MatDialog } from '@angular/material';

@Component({
    selector: 'app-transactions',
    templateUrl: './transactions.component.html',
    styleUrls: ['./transactions.component.scss']
})
export class TransactionsComponent implements OnInit {

    private single = [
        {
            name: 'Germany',
            value: 8940000
        },
        {
            name: 'USA',
            value: 5000000
        },
        {
            name: 'France',
            value: 7200000
        }
    ];

    private multi = [
        {
            name: 'Germany',
            'series': [
                {
                    name: '2010',
                    value: 7300000
                },
                {
                    name: '2011',
                    value: 8940000
                }
            ]
        },

        {
            name: 'USA',
            series: [
                {
                    name: '2010',
                    value: 7870000
                },
                {
                    name: '2011',
                    value: 8270000
                }
            ]
        },

        {
            name: 'France',
            series: [
                {
                    name: '2010',
                    value: 5000002
                },
                {
                    name: '2011',
                    value: 5800000
                }
            ]
        }
    ];

    view: any[] = [700, 400];

    // options
    showLegend = true;

    colorScheme = {
        domain: ['#F00', '#0F0', '#00F', '#AAAAAA']
    };

    // pie
    showLabels = true;
    explodeSlices = false;
    doughnut = false;


    private currentUser;
    private accountants: FilterModel;

    private curentDate: Date;
    private date = new Date();
    private filterForm: FormGroup;
    private tableData: TableDatas<Transaction>;
    private transactionTypes = [
        { id: 1, value: null, name: 'All' },
        { id: 2, value: true, name: 'Earning' },
        { id: 3, value: false, name: 'Spending' }
    ];

    private accountantId: number;
    private transactionType: boolean = null;
    private expensesType: number;

    private dates = [
        { value: this.date, name: 'Date' },
        { value: new Date().setDate(this.date.getDate() - 7), name: 'Week' },
        { value: new Date().setMonth(this.date.getMonth() - 1), name: 'Month' },
        { value: new Date().setFullYear(this.date.getFullYear() - 1), name: 'Year' }
    ];

    private columns = [
        { columnDef: 'id', header: 'Id', cell: (row: Transaction) => row.id },
        { columnDef: 'size', header: 'Size', cell: (row: Transaction) => row.size },
        {
            columnDef: 'dateOfTransaction', header: 'Date', cell: (row: Transaction) =>
                this.datePipe.transform(row.dateOfTransaction, 'medium')
        },
        {
            columnDef: 'transactionType', header: 'Transaction Type', cell: (row: Transaction) =>
                row.transactionType ? 'Earning' : 'Spending'
        },
        { columnDef: 'expensesType', header: 'Expenses Type', cell: (row: Transaction) => row.expensesType.name }
    ];

    constructor(private transactionService: TransactionService,
                private accountantService: AccountantService,
                private expensesTypeService: ExpensesTypeService,
                private authenticationService: AuthenticationService,
                private datePipe: DatePipe,
                private fb: FormBuilder,
                private addTransactionDialog: MatDialog) {

        this.authenticationService.currentUser.subscribe(u => this.currentUser = u);
    }

    ngOnInit(): void {
        this.setDefaultData();
        this.getTransactions();
        this.getAccountantsList();
        this.getExpensesTypes();
    }

    setDefaultData() {
        this.filterForm = this.fb.group({
            accountantId: ['', Validators.required],
            expensesTypeId: ['', Validators.required],
            transactionType: ['', Validators.required],
            date: ['', Validators.required]
        });

        const toSelect = this.dates.find(c => c.name == 'Date');
        this.filterForm.get('date').setValue(toSelect.value);
        this.curentDate = this.filterForm.get('date').value;
    }

    getAccountantsList() {
        this.accountantService.getAccountantsList(this.currentUser.id).subscribe((data: FilterModel) => {
            this.accountants = data;
            console.log(this.accountants);
        });
    }

    getExpensesTypes() {
        this.expensesTypeService.getExpensesTypes().subscribe((data: FilterModel) => {
        });
    }

    getTransactions() {
        this.transactionService.getTransactions(this.accountantId, this.curentDate.toUTCString(), this.transactionType, 1)
            .subscribe((data: TableDatas<Transaction>) => {
                this.tableData = data;
            });
    }

    AddTransaction() {
        const addTransactionDialogRef = this.addTransactionDialog.open(AddTransactionComponent, {
            disableClose: true
        });

        addTransactionDialogRef.afterClosed().subscribe(result => {
        });
    }

    updateData(tableProperty: TableProperty) {
        this.getTransactions();
    }

    changeDate($event) {
        this.curentDate = new Date($event.value);
        this.getTransactions();
    }

    changeAccountant($event) {
        this.accountantId = $event.value;
        this.getTransactions();
    }

    changeTransactionType($event) {
        this.transactionType = $event.value;
        this.getTransactions();
    }

    changeExpensesType($event) {
        this.expensesType = $event.value;
        this.getTransactions();
    }
}
