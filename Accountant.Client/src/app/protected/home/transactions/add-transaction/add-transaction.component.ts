import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material';
import { TransactionService } from '@app/_services';

@Component({
    selector: 'app-add-transaction',
    templateUrl: './add-transaction.component.html',
    styleUrls: ['./add-transaction.component.scss']
})
export class AddTransactionComponent implements OnInit {

    private addTransactionForm: FormGroup;
    private accountantId;
    private transactionTypes = [
        { id: 1, value: true, name: 'Earning' },
        { id: 2, value: false, name: 'Spending' }
    ];
    private expensesTypes = [
        { id: 1, name: 'Test' },
        { id: 2, name: 'New' }
    ];

    constructor(private formBuilder: FormBuilder,
                private transactionService: TransactionService,
                private dialogRef: MatDialogRef<AddTransactionComponent>) { }

    ngOnInit(): void {
        this.createForm();
    }

    get formControls() { 
        return this.addTransactionForm.controls; 
    }

    onSubmit() {
        this.closeDialog();
    }

    createForm() {
        this.addTransactionForm = this.formBuilder.group({
            size: ['', Validators.required],
            transactionType: ['', Validators.required],
            expensesType: ['', Validators.required]
        });
    }

    closeDialog() {
        this.dialogRef.close()
    }
}
