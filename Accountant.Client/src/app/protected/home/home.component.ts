import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '@app/_services';
import { Role } from '@app/_models';

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

    private currentUser;

    constructor(private authenticationService: AuthenticationService) {
        this.authenticationService.currentUser.subscribe(u => this.currentUser = u);
    }

    ngOnInit(): void {}

    get getRoleAcces(): String {
        let acc: String = "";

        this.currentUser.roles.forEach(role => {
            if(role.name == Role.User)
                acc += 'user ';
            else if(role.name == Role.Admin)
                acc += 'admin ';
        });
        return acc;
    }
}
