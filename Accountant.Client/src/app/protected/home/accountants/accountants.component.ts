import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { AddAccountantComponent } from './add-accountant/add-accountant.component';
import { AccountantService, AuthenticationService } from '@app/_services';
import { Accountant, TableProperty, TableDatas } from '@app/_models';
import { DatePipe } from '@angular/common';

@Component({
    selector: 'app-accountants',
    templateUrl: './accountants.component.html',
    styleUrls: ['./accountants.component.scss']
})
export class AccountantsComponent implements OnInit {

    private currentUser;
    private tableData: TableDatas<Accountant>;

    private columns = [
        { columnDef: 'id', header: 'Id', cell: (row: Accountant) => row.id },
        { columnDef: 'number', header: 'Number', cell: (row: Accountant) => row.number },
        { columnDef: 'balance', header: 'Balance', cell: (row: Accountant) => row.balance },
        { columnDef: 'dateOfTransaction', header: 'Date', cell: (row: Accountant) => this.datePipe.transform(row.dateCreated, 'medium') }
    ];

    constructor(private addAccountantDialog: MatDialog,
                private accountantService: AccountantService,
                private authenticationService: AuthenticationService,
                private datePipe: DatePipe) {
        this.authenticationService.currentUser.subscribe(u => this.currentUser = u);
    }

    ngOnInit(): void {
        this.getAccountants();
    }

    AddAccountant() {
        const addAccountantDialogRef = this.addAccountantDialog.open(AddAccountantComponent, {
            disableClose: true
        });

        addAccountantDialogRef.afterClosed().subscribe(result => {
            this.getAccountants();
        });
    }

    getAccountants() {
        this.accountantService.getAccountants(this.currentUser.id).subscribe((data: TableDatas<Accountant>) => {
            this.tableData = data;
        });
    }

    updateData(tableProperty: TableProperty) {
        this.accountantService.getAccountants(this.currentUser.id, tableProperty.searchTerm, tableProperty.orderBy, tableProperty.order, tableProperty.pageIndex, tableProperty.pageSize).subscribe((data: TableDatas<Accountant>) => {
            this.tableData = data;
        });
    }
}
