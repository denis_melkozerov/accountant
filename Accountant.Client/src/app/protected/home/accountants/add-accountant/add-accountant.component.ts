import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AccountantService, AuthenticationService } from '@app/_services';
import { MatDialogRef } from '@angular/material';

@Component({
    selector: 'app-add-accountant',
    templateUrl: './add-accountant.component.html',
    styleUrls: ['./add-accountant.component.scss']
})
export class AddAccountantComponent implements OnInit {

    addAccountantForm: FormGroup;

    constructor(private formBuilder: FormBuilder,
                private accountantService: AccountantService,
                private dialogRef: MatDialogRef<AddAccountantComponent>,
                private authenticationService: AuthenticationService) { }

    ngOnInit(): void {
        this.createForm();
     }

    get formControls() { 
        return this.addAccountantForm.controls; 
    }

    onSubmit() {
        this.accountantService.createAccountant(this.addAccountantForm.value).subscribe(data => {
            this.closeDialog();
        })
    }

    createForm() {
        this.addAccountantForm = this.formBuilder.group({
            number: ['', Validators.required],
            balance: ['', Validators.required],
            userId: [this.authenticationService.currentUserValue.id]
        });
    }

    closeDialog() {
        this.dialogRef.close()
    }
}
