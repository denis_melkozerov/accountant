import { Component, OnInit } from '@angular/core';
import { User, TableProperty, TableDatas } from '@app/_models';
import { MatDialog } from '@angular/material';
import { UserService, RoleService } from '@app/_services';
import { EditUserRolesComponent } from './edit-user-roles/edit-user-roles.component';

@Component({
    selector: 'app-users',
    templateUrl: './users.component.html',
    styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {

    private roles;
    private tableData: TableDatas<User>;
    private columns = [
        { columnDef: 'id', header: 'Id', cell: (row: User) => row.id },
        { columnDef: 'firstName', header: 'First Name', cell: (row: User) => row.firstName },
        { columnDef: 'lastName', header: 'Last Name', cell: (row: User) => row.lastName },
        { columnDef: 'email', header: 'Email', cell: (row: User) => row.email },
        { columnDef: 'roles', header: 'Roles', cell: (row: User) => row.roles.map(r => r.name).join(', ') },
        { columnDef: 'edit', header: 'Edit', cell: (row: User) => this.editUserRoles(row), template: true }
    ];

    constructor(private userService: UserService,
                public editUserRolesDialog: MatDialog,
                private roleService: RoleService) {
    }

    ngOnInit(): void {
        this.getRoles();
        this.getUsers();
    }

    getRoles() {
        this.roleService.getRoles().subscribe(data => {
            this.roles = data;
        });
    }

    getUsers() {
        this.userService.getUsers().subscribe((data: TableDatas<User>) => {
            this.tableData = data;
        });
    }

    updateData(tableProperty: TableProperty) {
        this.userService.getUsers(tableProperty.searchTerm, tableProperty.orderBy, tableProperty.order, tableProperty.pageIndex, tableProperty.pageSize)
        .subscribe((data: TableDatas<User>) => {
            this.tableData = data;
        });
    }

    editUserRoles(row) {
        const addAccountDialogRef = this.editUserRolesDialog.open(EditUserRolesComponent, {
            disableClose: true,
            data: { 
                row: row, 
                roles: this.roles 
            }
        });

        addAccountDialogRef.afterClosed().subscribe(result => {
        });
    }
}
