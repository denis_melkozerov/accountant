import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthenticationService } from '@app/_services';
import { MatDialogRef } from '@angular/material';

@Component({
    selector: 'app-edit-user',
    templateUrl: './edit-user.component.html',
    styleUrls: ['./edit-user.component.scss']
})
export class EditUserComponent implements OnInit {

    private editRoleForm: FormGroup;
    private currentUser;
    
    constructor(private formBuilder: FormBuilder,
        private authenticationService: AuthenticationService,
        private dialogRef: MatDialogRef<EditUserComponent>) { 
            this.authenticationService.currentUser.subscribe(u => this.currentUser = u);
    }

    ngOnInit(): void {
        this.createForm();
    }

    onSubmit() {
        this.closeDialog();
    }

    createForm() {
        this.editRoleForm = this.formBuilder.group({
            firstName: [this.currentUser.firstName, Validators.required],
            lastName: [this.currentUser.lastName, Validators.required],
            login: [this.currentUser.login, Validators.required],
            email: [this.currentUser.email, Validators.required]
        });
    }

    closeDialog() {
        this.dialogRef.close()
    }
}
