import { Component, OnInit, Inject, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { MAT_DIALOG_DATA, MatAutocomplete, MatAutocompleteSelectedEvent } from '@angular/material';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { Roles, UserRoles, UpdateUserRoles } from '@app/_models';
import { RoleService } from '@app/_services';

@Component({
	selector: 'app-edit-user-roles',
	templateUrl: './edit-user-roles.component.html',
	styleUrls: ['./edit-user-roles.component.scss']
})
export class EditUserRolesComponent implements OnInit {

	editUserForm: FormGroup;

	visible = true;
	selectable = true;
	removable = true;
	addOnBlur = true;
	separatorKeysCodes: number[] = [ENTER, COMMA];
	newUserRoles: Array<UserRoles>;
	userRoles;
	allRoles;

	@ViewChild('roleInput', { static: false }) roleInput: ElementRef<HTMLInputElement>;
	@ViewChild('auto', { static: false }) matAutocomplete: MatAutocomplete;

	constructor(private formBuilder: FormBuilder, @Inject(MAT_DIALOG_DATA) public data: any,
	private roleService: RoleService) {
	}

	ngOnInit(): void {
		this.userRoles = this.data.row.roles;
		this.allRoles = this.data.roles;
		this.createForm();
	}

	onSubmit() {
		let opdateUserRoles: UpdateUserRoles = new UpdateUserRoles();
		opdateUserRoles.userId = this.data.row.id;
		this.userRoles.forEach(role => {
			opdateUserRoles.roleIds.push(role.id);
		});
		this.roleService.updateRoles(opdateUserRoles).subscribe(data => {
			this.data.row.roles = data;
		});
	}

	get formControls() {
		return this.editUserForm.controls;
	}

	createForm() {
		this.editUserForm = this.formBuilder.group({
			firstName: [this.data.row.firstName],
			lastName: [this.data.row.lastName],
			email: [this.data.row.email],
			roleCtrl: []
		});
	}

	remove(role: Roles): void {
		const index = this.userRoles.indexOf(role);

		if (index >= 0) {
			this.userRoles.splice(index, 1);
		}
	}

	selected(event: MatAutocompleteSelectedEvent): void {
		this.userRoles.push(event.option.value);
		this.roleInput.nativeElement.value = '';
		this.editUserForm.controls.roleCtrl.setValue(null);
	}
}
